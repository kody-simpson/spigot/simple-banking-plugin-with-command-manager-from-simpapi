package me.kodysimpson.simplebankingcommandmanager;

import me.kodysimpson.simpapi.colors.ColorTranslator;
import me.kodysimpson.simpapi.command.CommandList;
import me.kodysimpson.simpapi.command.CommandManager;
import me.kodysimpson.simpapi.command.SubCommand;
import me.kodysimpson.simplebankingcommandmanager.commands.DepositCommand;
import me.kodysimpson.simplebankingcommandmanager.commands.ReportCommand;
import me.kodysimpson.simplebankingcommandmanager.commands.WithdrawCommand;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Color;
import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

public final class SimpleBankingCommandManager extends JavaPlugin {

    private static final Logger log = Logger.getLogger("Minecraft");
    private static Economy econ = null;

    @Override
    public void onEnable() {
        // Plugin startup logic

        if (!setupEconomy() ) {
            log.severe(String.format("[%s] - Disabled due to no Vault dependency found!", getDescription().getName()));
            getServer().getPluginManager().disablePlugin(this);
            return;
        }

        //Register the subcommands under a core command
        try {
            CommandManager.createCoreCommand(this, "simplebanking", "Store and retrieve money", "/simplebanking", new CommandList() {
                @Override
                public void displayCommandList(Player p, List<SubCommand> subCommandList) {
                    p.sendMessage(ColorTranslator.translateColorCodes("&e-------------------------------"));
                    subCommandList.forEach(subCommand -> {
                                p.sendMessage(ColorTranslator.translateColorCodes("&a" + subCommand.getSyntax() + " - &f" + subCommand.getDescription()));
                            });
                    p.sendMessage(ColorTranslator.translateColorCodes("&e-------------------------------"));
                }
            }, Arrays.asList("sb", "bank"), DepositCommand.class, WithdrawCommand.class, ReportCommand.class);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic

    }

    private boolean setupEconomy() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        econ = rsp.getProvider();
        return econ != null;
    }

    public static Economy getEconomy() {
        return econ;
    }

}
