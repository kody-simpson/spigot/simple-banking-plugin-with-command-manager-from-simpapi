package me.kodysimpson.simplebankingcommandmanager.commands;

import me.kodysimpson.simpapi.colors.ColorTranslator;
import me.kodysimpson.simpapi.command.SubCommand;
import me.kodysimpson.simplebankingcommandmanager.SimpleBankingCommandManager;
import net.milkbowl.vault.economy.EconomyResponse;
import org.bukkit.entity.Player;

import java.util.List;

public class DepositCommand extends SubCommand {

    @Override
    public String getName() {
        return "deposit";
    }

    @Override
    public String getDescription() {
        return "Deposit money to your account.";
    }

    @Override
    public String getSyntax() {
        return "/simplebanking deposit <amount>";
    }

    @Override
    public void perform(Player player, String[] args) {

        if (args.length > 1){
            //grab the amount to deposit from the args
            double amount = Double.parseDouble(args[1]);

            //make sure it is greater than zero
            if (amount <= 0){
                player.sendMessage(ColorTranslator.translateColorCodes("&4You must deposit a value greater than zero."));
            }else{
                EconomyResponse response = SimpleBankingCommandManager.getEconomy().depositPlayer(player, amount);
                if (response.type == EconomyResponse.ResponseType.SUCCESS){
                    player.sendMessage(ColorTranslator.translateColorCodes("&b" + amount + " has been deposited into your account. The new total is " + response.balance));
                }else{
                    player.sendMessage(ColorTranslator.translateColorCodes("&4Your deposit could not be processed. Try again later."));
                }
            }
        }else{
            player.sendMessage(ColorTranslator.translateColorCodes("&4You must deposit a value greater than zero."));
            player.sendMessage(ColorTranslator.translateColorCodes("&aExample: &e/simplebanking deposit 123.4"));
        }

    }

    @Override
    public List<String> getSubcommandArguments(Player player, String[] args) {
        return null;
    }
}
