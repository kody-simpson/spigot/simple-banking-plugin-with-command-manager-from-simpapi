package me.kodysimpson.simplebankingcommandmanager.commands;

import me.kodysimpson.simpapi.colors.ColorTranslator;
import me.kodysimpson.simpapi.command.SubCommand;
import me.kodysimpson.simplebankingcommandmanager.SimpleBankingCommandManager;
import org.bukkit.entity.Player;

import java.util.List;

public class ReportCommand extends SubCommand {
    @Override
    public String getName() {
        return "report";
    }

    @Override
    public String getDescription() {
        return "Report your current balance.";
    }

    @Override
    public String getSyntax() {
        return "/simplebanking report";
    }

    @Override
    public void perform(Player player, String[] args) {

        double balance = SimpleBankingCommandManager.getEconomy().getBalance(player);
        player.sendMessage(ColorTranslator.translateColorCodes("&aYour current account balance is: &b&l$" + balance));

    }

    @Override
    public List<String> getSubcommandArguments(Player player, String[] args) {
        return null;
    }
}
