package me.kodysimpson.simplebankingcommandmanager.commands;

import me.kodysimpson.simpapi.colors.ColorTranslator;
import me.kodysimpson.simpapi.command.SubCommand;
import me.kodysimpson.simplebankingcommandmanager.SimpleBankingCommandManager;
import net.milkbowl.vault.economy.EconomyResponse;
import org.bukkit.entity.Player;

import java.util.List;

public class WithdrawCommand extends SubCommand {
    @Override
    public String getName() {
        return "withdraw";
    }

    @Override
    public String getDescription() {
        return "Take money from your account.";
    }

    @Override
    public String getSyntax() {
        return "/simplebanking withdraw 100";
    }

    @Override
    public void perform(Player player, String[] args) {

        if (args.length > 1){
            //grab the amount to withdraw from the args
            double amount = Double.parseDouble(args[1]);

            //make sure it is greater than zero
            if (amount <= 0){
                player.sendMessage(ColorTranslator.translateColorCodes("&4You must withdraw a value greater than zero."));
            }else{
                EconomyResponse response = SimpleBankingCommandManager.getEconomy().withdrawPlayer(player, amount);
                if (response.type == EconomyResponse.ResponseType.SUCCESS){
                    player.sendMessage(ColorTranslator.translateColorCodes("&e" + amount + " has been withdrawn from your account. The new total is " + response.balance));
                }else{
                    player.sendMessage(ColorTranslator.translateColorCodes("&4Your withdrawal could not be processed. Try again later."));
                }
            }
        }else{
            player.sendMessage(ColorTranslator.translateColorCodes("&4You must withdraw a value greater than zero."));
            player.sendMessage(ColorTranslator.translateColorCodes("&aExample: &e/simplebanking withdraw 123.4"));
        }

    }

    @Override
    public List<String> getSubcommandArguments(Player player, String[] args) {
        return null;
    }
}
